# Telegram Web App

[![pipeline status](https://gitlab.com/patojad/telegram/badges/master/pipeline.svg)](https://gitlab.com/patojad/telegram/commits/master)

### Detalles Tecnicos

La aplicación se basa en ReactJS, JavaScript y TDLib (biblioteca de base de datos de Telegram) compilada en WebAssembly.

### Corriendolo local

Instalar [node.js](http://nodejs.org/).

Instalar dependencias con

```lang=bash
npm install
```

Esto instalará todas las dependencias necesarias.

Todos los archivos TDLib se instalarán en la carpeta node_modules/tdweb/dist/ Cópielos manualmente en la carpeta pública con:

```lang=bash
cp node_modules/tdweb/dist/* public/
```


Ejecute la aplicación en modo de desarrollo con:

```lang=bash
npm run start
```

Open http://localhost:3000 to view it in the browser.

### Deploying to GitHub Pages

1. **Update *homepage* property at the app's `package.json` file.**

Define its value to be the string `http://{username}.github.io/{repo-name}`, where `{username}` is your GitHub username, and `{repo-name}` is the name of the GitHub repository. Since my GitHub username is `evgeny-nadymov` and the name of my GitHub repository is `telegram-react`, I added the following property:

```js
//...
"homepage": "http://evgeny-nadymov.github.io/telegram-react"
```

2. **Generate a *production build* of your app and deploy it to GitHub Pages.**

```
$ npm run deploy
```
### References

Abra http://localhost:3000 para verlo en el navegador.
